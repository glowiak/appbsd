PREFIX=	/usr/local
DESTDIR=	

all:
	@echo targets: install

install:
	mkdir -pv ${DESTDIR}${PREFIX}/appbsd/lib
	mkdir -pv ${DESTDIR}${PREFIX}/appbsd/appdir
	mkdir -pv ${DESTDIR}${PREFIX}/bin
	cp -v ./src/libappbsd-run.sh ${DESTDIR}${PREFIX}/appbsd/lib/libappbsd-run.sh
	chmod 775 ${DESTDIR}${PREFIX}/appbsd/lib/libappbsd-run.sh
	cp -v ./src/appbsd-run.sh ${DESTDIR}${PREFIX}/bin/appbsd-run
	chmod 775 ${DESTDIR}${PREFIX}/bin/appbsd-run
	cp -v ./src/appbsd-create.sh ${DESTDIR}${PREFIX}/bin/appbsd-create
	chmod 775 ${DESTDIR}${PREFIX}/bin/appbsd-create
	mkdir -pv ${DESTDIR}${PREFIX}/share/applications
	cp -v ./src/appbsd.png ${DESTDIR}${PREFIX}/appbsd/appbsd.png
	cp -v ./src/appbsd.desktop ${DESTDIR}${PREFIX}/share/applications/appbsd.desktop
	chmod 775 ${DESTDIR}${PREFIX}/share/applications/appbsd.desktop
	cp -v ./src/libappbsd-extract.sh ${DESTDIR}${PREFIX}/appbsd/lib/libappbsd-extract.sh
	chmod 775 ${DESTDIR}${PREFIX}/appbsd/lib/libappbsd-extract.sh
	cp -v ./src/appbsd-extract.sh ${DESTDIR}${PREFIX}/bin/appbsd-extract
	chmod 775 ${DESTDIR}${PREFIX}/bin/appbsd-extract
	cp -v ./src/appbsd-extract.desktop ${DESTDIR}${PREFIX}/share/applications/appbsd-extract.desktop
	chmod 775 ${DESTDIR}${PREFIX}/share/applications/appbsd-extract.desktop
	cp -v ./src/appbsd-launcher.sh ${DESTDIR}${PREFIX}/bin/appbsd-launcher
	chmod 775 ${DESTDIR}${PREFIX}/bin/appbsd-launcher
	cp -v ./src/appbsd-launcher.desktop ${DESTDIR}${PREFIX}/share/applications/appbsd-launcher.desktop
	chmod 775 ${DESTDIR}${PREFIX}/share/applications/appbsd-launcher.desktop
	cp -v ./src/abl-template.desktop ${DESTDIR}${PREFIX}/appbsd/abl-template.desktop
	cp -v ./src/app2run.sh ${DESTDIR}${PREFIX}/bin/app2run
	chmod 775 ${DESTDIR}${PREFIX}/bin/app2run
