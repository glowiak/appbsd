<p align="center">
	<img src="https://codeberg.org/glowiak/appbsd/raw/branch/master/appbsd_logo.png">
</p>

# AppBSD

AppBSD - AppImage like thing for FreeBSD

Aim of this project is to create a flexible system of portable applications for FreeBSD as AppImage is not ported to this OS.

Though it will probably become useless, when 13.1-RELEASE came out, it's still fine that it does not require Linux emulation

### Dependiences

doas installed and preconfigured with 'nopass' and 'keepenv' options for your user

shuf installed, to generate random numbers

ext2fs kernel module loaded on boot

e2fsprogs to use mke2fs needed to create app images

gsed installed to make appbsd-launcher work

base64 installed (0.4.9 and newer)

### How does it work

AppBSD works more like AppImage than like macos/helloSystem apps. Application is not a directory, nor an executable. The Application is a raw data disk foratted as ext2. When launching it with appbsd-run, the image is being connected to /dev/md(random number) via FreeBSD's native mdconfig app (as root, that's why we need doas) and then mounts it to temportary read-write directory in /usr/local/appbsd/appdir/app(random number), and starts AppRun script inside it. After the application stops, it unmounts the directory, disconnects the virtual device and removes the temportary directory.

### Features


[DONE] running apps

[DONE] read-write support

[DONE] easily creating app images

[DONE] integration with ./

[DONE] parsing arguments to apps

[DONE] Extracting app images

[DONE] Creating menuentries for App Images

### Installation

Download latest release, untargzip it and type 'make install' as root.

### Running Apps

You need to run apps as normal user with doas configured, shuf installed and ext2fs kernel module loaded.

Created with 0.4.8 or older: "appbsd-run /path/to/app.App"

Created with 0.4.9 and newer: "./app.App"

You can also run 0.4.9 apps with appbsd-run command, but ./ is recommended.

A note that 0.4.9 can run 0.4.8 apps, but 0.4.8 CANNOT run 0.4.9 apps, so make sure you've updated AppBSD.

If you are scared of the Terminal, you can just set the AppBSD program to open .App files and just double-click it to run

### Testing apps

Here are some prebuilt 13.0 x64 Images (in the 0.4.9 format):

https://drive.google.com/drive/folders/1qkNb2Kh8v6VEkBylRecO4E5y8Ye4pMsu?usp=sharing

### Creating apps

as from version 0.2 you can now create your own app images!

You need e2fsprogs package;

Make a directory with all the app files (usr,usr/local etc) and the AppRun script with chmod 775 did on it.

The AppRun file should look like this:

	#!/bin/sh
	usr/local/bin/program $*

Then run 'env APP_SIZE=size_of_app_in_bytes appbsd-create /path/to/new/app/image.App', to check size of your app execute 'du -h .' in app directory. Always you should give some more space than the app takes. Geany for example takes 14 megabytes, so 15000 dd bytes, but always better is to set it bit higher, like 18000 like I did.

And remember to share your AppBSD Image with others :)

### Extracting apps

Want to see inside an app image? or add new files and repack? If you have version 0.4.4 or greater, you can extract app images.

type in terminal 'appbsd-extract /path/to/app.App' or run app with 'AppBSD Extractor' program. An 'appbsd-root' directory will be created and there will be contents of the image.

### Menuentries

Are you tired of navigating by thousands of directories to find your apps? v0.4.6 is here and brings a solution - appbsd-launcher tool similar to AppImageLauncher.

It creates a desktop entry for an app image, based on it's name, so for example multimc-0.6.13 for the mmc, so if you want to have all you apps named correctly, please rename it to something like MultiMC.App before running it. The desktop entry will be in 'AppBSD Images' menu.

Using: 'appbsd-launcher /path/to/app.App' or open an image with the "AppBSD Launcher" app.

### Converting Apps to New Format

Want to run apps by just typing ./app.App? v0.4.9 brings this feature to you!

To convert an older Image to new format, just type:

		app2run /path/to/app.App

This will generate a app.App.run.App file, which is the executable version of your app.

It still needs AppBSD to be installed however.

### Tools

Package comes with several tools. Here's a quick list of them:

1. appbsd-run - runs app images

2. appbsd-extract - extracts app images

3. appbsd-create - creates app images

4. appbsd-launcher - creates menuentries for app images

5. app2run - converts Apps to new 0.4.9 format

### Accessing Application Data

In some cases, you need to access the data application comes with.

You can access it via AppDirs - mounted images of App, somewhere on your disk. It's by default /usr/local/appbsd/appdir/app(number of the app, it's completly random) or /usr/local/appbsd/appdir/static/name_of_image_without_extension
