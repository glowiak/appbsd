#!/bin/sh
if [ "$1" == "--help" ]
then
	echo "appbsd-run - run AppBSD Images"
	echo "Usage: appbsd-run /path/to/app.App"
	exit 0
fi
if [ "$1" == "" ]
then
	echo "ERROR: empty arguments"
	exit 1
fi
# detect if using the New Format
if [ ! "$(cat $1 | grep PROGAS)" == "" ]
then
	$(readlink -f $*) $(echo $* | sed "s|$1||")
	exit 0
fi
RANDOM_NR="$(shuf -i 0-100 -n1)"
RANDOM="$RANDOM_NR" /usr/local/appbsd/lib/libappbsd-run.sh $*
sleep 0.1
doas umount /usr/local/appbsd/appdir/app$RANDOM_NR
doas mdconfig -d -u $RANDOM_NR
doas rm -r /usr/local/appbsd/appdir/app$RANDOM_NR
