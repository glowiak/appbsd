#!/bin/sh
# AppBSD - AppImage for FreeBSD
if [ "$1" == "--help" ]
then
	echo "appbsd-create - create AppBSD Images"
	echo "Usage: env APP_SIZE=size appbsd-create /path/to/app.App"
	echo "To count APP_SIZE, go to root directory of your app (you need usr directory structure), and type 'du -h .'"
	echo "This will list you size of all your files, and the main size at the bottom."
	echo "Then change it to dd bytes. For example 14 megabytes is about 15000 dd bytes, but it's not so precise, so make the image"
	echo "quite larget, to make sure that all files will fit on it."
	exit 0
fi
APP_MK_DIR=.
APP_FILE="$*"
RANDOM="$(shuf -i 0-100 -n1)"
# echo "$RANDOM_2" > /tmp/rand2
# RANDOM="$(cat /tmp/rand2)"
# RANDOM="$(date | base64 | sed 's/=//g')"
APP_DIR="/usr/local/appbsd/appdir/app${RANDOM}"
APP_DEVICE="/dev/md${RANDOM}"

error()
{
	echo "ERROR: $*"
	exit 1
}

echo "Creating AppBSD Image $APP_FILE..."
if [ "$*" == "" ]
then
	error "Please enter correct filename for your Image. It should end with .App extension."
	exit 0
fi
if [ -z "$APP_SIZE" ]
then
	echo "Please specify image size with 'env APP_SIZZE=size_in_bytes appbsd-create'"
	exit 1
fi
if [ ! -f "./AppRun" ]
then
	error "AppRun file couldn't be found. Please create it and do chmod 775 on it!"
	exit 1
fi
doas rm -rf /tmp/apptmp${RANDOM}
doas mkdir -p /tmp/apptmp${RANDOM}


echo "Creating Image with size of ${APP_SIZE}..."
dd if=/dev/zero of="$APP_FILE" bs=1k count=$APP_SIZE

echo "Assigning image to virtual device /dev/md${RANDOM}..."
doas mdconfig -a -t vnode -f "$APP_FILE" -u $RANDOM

echo "Creating an ext2 file system on the Image..."
doas mke2fs $APP_DEVICE

echo "Mounting Image to temportary location..."
doas mount -t ext2fs $APP_DEVICE /tmp/apptmp${RANDOM}

echo "Copying Application contents onto the Image..."
doas cp -rfv * /tmp/apptmp${RANDOM}/
# doas cp -rfv ./.* /tmp/apptmp${RANDOM}/

echo "Unmounting the Image..."
doas umount /tmp/apptmp${RANDOM}
echo "Unassigning virtual node..."
doas mdconfig -d -u $RANDOM

# echo "AppBSD Image ${APP_FILE} created. Enjoy!"
doas rm -rf /tmp/apptmp${RANDOM}

# convert package to the new format and move to a correct plate
NEW_APPFILE=$(app2run ${APP_FILE})
rm -f ${APP_FILE}
mv ${NEW_APPFILE} ${APP_FILE}
echo "AppBSD Image ${APP_FILE} created. Enjoy!"

exit 0
