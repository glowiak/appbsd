#!/bin/sh
# AppBSD - AppImage for FreeBSD
APP="$(readlink -f $*)"
RANDOM="$(date | base64 | sed 's/=//g')"
APP_DIR="/usr/local/appbsd/appdir/app${RANDOM}"
APP_DEVICE="/dev/md${RANDOM}"

appstart_bsd()
{
	case $(whoami) in
		root)
			$*
			;;
		*)
			if which doas;
			then
				doas $*
			fi
			if which sudo;
			then
				sudo $*
			fi
			if which su;
			then
				su root -c "$*"
			fi
			;;
	esac
}
