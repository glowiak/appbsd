#!/bin/sh
# AppBSD - AppImage for FreeBSD
APP="$(readlink -f $1)"
APP_ARGS=$(echo $* | sed "s|$1||")
APP_DIR="/usr/local/appbsd/appdir/app${RANDOM}"
APP_DEVICE="/dev/md${RANDOM}"
APP_STATIC="$(echo $(basename $APP) | sed 's/.App//')"

error()
{
	echo "$*"
	exit 1
}
# test App
if [ ! -f "$APP" ]
then
	echo "ERROR: Not a file!"
	exit 1
fi
# bind image to a virtual device
doas mdconfig -a -t vnode -f "$APP" -u "$RANDOM" || error "Couldn't mount Image"
# mount the app
doas mkdir -p "$APP_DIR"
doas mount -t ext2fs "$APP_DEVICE" "$APP_DIR"
cd "$APP_DIR"
doas chown -R ${USER}:${USER} $APP_DIR/*
# make static appdir
export APPDIR_STATIC=/usr/local/appbsd/appdir/static/$APP_STATIC
if [ ! -d $APPDIR_STATIC ]
then
	doas ln -s $APP_DIR $APPDIR_STATIC
fi
if [ -d $APPDIR_STATIC ]
then
	echo "WARNING: Failed to create a static AppDir: directory exists"
fi
# start the App
"$APP_DIR/AppRun" $APP_ARGS
# unlink and remove the static appdir
doas unlink $APPDIR_STATIC
# doas rm -r $APPDIR_STATIC
