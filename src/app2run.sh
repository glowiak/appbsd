#!/bin/sh
APP=$1
DEST=$1.run.App
APP_NAME=$(echo $(basename $APP) | sed 's/.App//')
if [ "$1" == "--help" ]
then
	echo "app2run - convert AppBSD images to new format"
	echo "usage: sh $0 /path/to/app.App"
	exit 0
fi
if [ ! -f "$APP" ]
then
	echo "file not found"
	exit 1
fi
# echo "Converting $APP ($APP_NAME) to $DEST"
cat >> $DEST << "EOF"
#!/bin/sh
if ! command -v appbsd-run
then
	echo "appbsd is required to run this app"
	exit 1
fi
PROG="
EOF
cat $APP | base64 >> $DEST
echo '"' >> $DEST
echo "PRGNAM=$APP_NAME" >> $DEST
cat >> $DEST << "EOF"
PROGAS=${PRGNAM}$(shuf -i 0-100 -n1)
rm -f /tmp/$PROGAS.App
echo "$PROG" | base64 -d > /tmp/$PROGAS.App
appbsd-run /tmp/$PROGAS.App $*
rm -f /tmp/$PROGAS.App
EOF
chmod 775 $DEST
echo "$(readlink -f $DEST)"
