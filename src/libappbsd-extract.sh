#!/bin/sh
# AppBSD - AppImage for FreeBSD
APP="$(readlink -f $1)"
APP_DIR="/usr/local/appbsd/appdir/app${RANDOM}"
APP_DEVICE="/dev/md${RANDOM}"
EXTRACT_APP_DIR="$(dirname $(readlink -f $APP))/appbsd-root"

rm -rf /tmp/appbsd-extract1.App

error()
{
	echo "$*"
	exit 1
}
# test App
if [ ! -f "$APP" ]
then
	echo "ERROR: Not a file!"
	exit 1
fi
case $FORMAT in
	NEW)
		. $APP
		echo "$PROG" | base64 -d > /tmp/appbsd-extract1.App
		APF=/tmp/appbsd-extract1.App
		;;
	OLD)
		APF=$APP
		;;
esac
# bind image to a virtual device
doas mdconfig -a -t vnode -f "$APF" -u "$RANDOM" || error "Couldn't mount Image"
# mount the app
doas mkdir -p "$APP_DIR"
doas mount -t ext2fs "$APP_DEVICE" "$APP_DIR"
# start the App
cd "$APP_DIR"
echo "Extracting AppBSD image into $EXTRACT_APP_DIR..."
mkdir -pv "$EXTRACT_APP_DIR"
cp -rfv $APP_DIR/* $EXTRACT_APP_DIR/
