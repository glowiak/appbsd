#!/bin/sh
if [ "$1" == "--help" ]
then
	echo "appbsd-launcher - create desktop entries for AppBSD Images"
	echo "Usage: appbsd-launcher /path/to/app.App"
	echo "Please have on mind that name of the app is the name of the file, without the .App extension, so you should change filename from name-ver.arch.App to name.App, to know what even is that"
	exit 0
fi
if [ "$1" == "" ]
then
	echo "ERROR: empty arguments"
	exit 1
fi
APP="$(readlink -f $1)"
TEMPLATE=/usr/local/appbsd/abl-template.desktop
APPS_DIR=/usr/local/share/applications
SED=gsed
APP_NAME="$(echo $(basename $APP) | sed 's/.App//')"
set -e

if [ ! -f "$APP" ]
then
	echo "App Image could not be found"
	exit 1
fi

echo "Creating a desktop entry for app image $APP..."

echo "App name: $APP_NAME"
echo "App path: $APP"

doas cp $TEMPLATE $APPS_DIR/$APP_NAME.abl.desktop
doas $SED -i "s|%%NAME%%|${APP_NAME}|" $APPS_DIR/$APP_NAME.abl.desktop
doas $SED -i "s|%%APP_FILE%%|${APP}|" $APPS_DIR/$APP_NAME.abl.desktop
doas chmod 775 $APPS_DIR/$APP_NAME.abl.desktop

echo "$APP --> $APPS_DIR/$APP_NAME.abl.desktop"
